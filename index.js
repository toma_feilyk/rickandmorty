const charactersElement = document.getElementById('characters');
const searchElement = document.getElementById('search');
const sortByDateAscElement = document.getElementById('sortByDateAsc');
const sortByDateDescElement = document.getElementById('sortByDateDesc');
const sortByEpisodesElement = document.getElementById('sortByEpisodes');
const resetButton = document.getElementById('reset');

function formatDate(date) {
  const dateObj = new Date(date);
  return dateObj.toLocaleString();
}

function compareDates(a, b) {
  const dateA = new Date(a),
    dateB = new Date(b);

  return dateA - dateB;
}

class CharactersService {
  constructor(root) {
    this.root = root; //root element for the characters list
    this.data = [];
    this.filteredData = [];
    this.loading = false; //is list fetching

    this.cursor = 0; //id of the last shown element
    this.count = 10; //amount of elements to show after fetching
    this.eol = false; //boolean flag to indicate that all items are displayed

    this.sortColumn = null; //sort by column (date/episodes)
    this.sortDir = null; //sort direction (asc/desc)

    this.query = ''; //search query
  }

  fetchData() {
    this.loading = true;

    const url = 'https://rickandmortyapi.com/api/character';
    return fetch(url)
      .then((res) => res.json())
      .then((data) => {
        this.data = this.mapDataToFrontend(data.results);
        this.filteredData = [...this.data];
        this.loading = false;

        return this.data;
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  mapDataToFrontend(rawData) {
    return rawData.map((character) => {
      //all needed data is listed below as an entity
      const { created, species, image, episode, name, location } = character;
      return { created, species, image, episode, name, location };
    });
  }

  get elements() {
    return this.filteredData.map(
      ({ created, species, image, episode, name, location }) => {
        //create element
        const element = this.createCharElement({
          created,
          species,
          image,
          episode,
          name,
          location,
        });

        return element;
      }
    );
  }

  createCharElement({ created, species, image, episode, name, location }) {
    const charElement = document.createElement('div');
    charElement.classList.add('character__block');

    //img
    const imageElement = document.createElement('img');
    imageElement.setAttribute('src', image);
    imageElement.classList.add('character__image');

    //name
    const nameElement = document.createElement('span');
    nameElement.innerHTML = name;
    nameElement.classList.add('character__name');

    //species
    const speciesElement = document.createElement('span');
    speciesElement.innerHTML = species;
    speciesElement.classList.add('character__species');

    //location
    const locationElement = document.createElement('span');
    locationElement.innerHTML = location.name;
    locationElement.classList.add('character__location');

    //block with img, name, species and location
    const headerElement = document.createElement('div');
    headerElement.classList.add('character__header');

    const mainInfoElement = document.createElement('div');
    mainInfoElement.classList.add('character__main-info');

    [nameElement, speciesElement, locationElement].forEach((element) =>
      mainInfoElement.appendChild(element)
    );

    [imageElement, mainInfoElement].forEach((element) =>
      headerElement.appendChild(element)
    );

    //created date
    const dateElement = document.createElement('span');
    dateElement.innerHTML = formatDate(created);
    dateElement.classList.add('character__date');

    //episode
    const episodesListElement = document.createElement('div');
    episodesListElement.classList.add('character__episodes__list');
    episodesListElement.innerHTML = `Episodes: ${episode
      .map((i) => i.replace('https://rickandmortyapi.com/api/episode/', ''))
      .join(', ')}`;

    [headerElement, dateElement, episodesListElement].forEach((element) =>
      charElement.appendChild(element)
    );

    return charElement;
  }

  search(query) {
    query = query.toLowerCase();
    if (this.loading || query == this.query) return;

    this.query = query;

    this.filteredData = this.data.filter((character) =>
      character.name.toLowerCase().includes(this.query)
    );
    this.sort({ column: this.sortColumn, direction: this.sortDir });

    this._updateElements();
  }

  sort({ column, direction }) {
    if (this.loading || !column) return;
    if (column == 'date' && !direction) return;
    if (this.sortColumn == column && this.sortDir == direction) return;

    this.sortColumn = column;
    this.sortDir = direction;

    this.filteredData.sort((a, b) => {
      if (this.sortColumn == 'date') {
        const compareValue = compareDates(a.created, b.created);
        return this.sortDir == 'asc' ? compareValue : compareValue * -1;
      } else if (this.sortColumn == 'episodes') {
        const compareValue = b.episode.length - a.episode.length;
        if (compareValue == 0) return -compareDates(a.created, b.created);
        else return compareValue;
      }
    });

    this._updateElements();
  }

  _updateElements() {
    const slice = this.elements.slice(0, this.cursor);
    this._clearList();
    this._renderElements(slice);
  }

  _renderElements(data) {
    data.map((element) => {
      this.root.appendChild(element);
    });
  }

  _clearList() {
    while (this.root.firstChild) {
      this.root.removeChild(this.root.lastChild);
    }
  }

  reset() {
    //don't clear the DOM if it's necessary only to scroll list to top
    if (
      !this.query &&
      !this.sortColumn &&
      !this.sortDir &&
      this.cursor == this.count
    ) {
      return;
    }

    this._clearList();

    this.cursor = 0;
    this.eol = false;

    this.filteredData = [...this.data];

    this.query = '';
    this.sortColumn = null;
    this.sortDir = null;

    this.renderMore();
  }

  renderMore() {
    if (this.loading || this.eol) return;

    const slice = this.elements.slice(this.cursor, this.cursor + this.count);
    this._renderElements(slice);

    if (slice.length + this.cursor == this.elements.length) {
      this.eol = true;
    }

    this.cursor += slice.length;
  }
}

const service = new CharactersService(charactersElement);

service.fetchData().then((_) => {
  service.renderMore();
  checkElementScroll(charactersElement);
});

charactersElement.addEventListener('scroll', function (event) {
  checkElementScroll(event.target);
});

searchElement.addEventListener('input', function (event) {
  const input = event.target;
  service.search(input.value.trim());
});

sortByDateAsc.addEventListener('click', function () {
  service.sort({ column: 'date', direction: 'asc' });
});

sortByDateDesc.addEventListener('click', function () {
  service.sort({ column: 'date', direction: 'desc' });
});

sortByEpisodes.addEventListener('click', function () {
  service.sort({ column: 'episodes', direction: null });
});

resetButton.addEventListener('click', function () {
  charactersElement.scrollTop = 0;
  service.reset();
});

function checkElementScroll(element) {
  const scrolled = Math.ceil(element.scrollTop + element.clientHeight);

  if (scrolled >= element.scrollHeight) {
    service.renderMore();
  }
}
